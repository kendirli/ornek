$('#products').DataTable({
    "language":{
        url:"/js/dataTableTr.json"
    },
    "paging": true,
    "lengthChange": false,
    "searching": true,
    "ordering": false,
    "autoWidth": true,
    "responsive": true,
});
$('#nasaImages').DataTable({
    "language":{
        url:"/js/dataTableTr.json"
    },
    "pageLength":5,
    "paging": true,
    "lengthChange": false,
    "searching": true,
    "ordering": false,
    "autoWidth": true,
    "responsive": true,
});
