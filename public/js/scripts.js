$("input[data-bootstrap-switch]").each(function(){
    $(this).bootstrapSwitch('state', $(this).prop('checked'));
})
function swalSweetAlert(params) {
    Swal.fire({
        title: 'Emin Misiniz?',
        text: "Kayıt Silinecektir!",
        icon: 'error',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Evet, Silinsin',
        cancelButtonText: 'Hayır'
    }).then((result) => {
        if (result.isConfirmed) {

            let url = params.forwardUrl;
            let response = ajaxRequest(url,params.data,'POST');
            swalFire(response);
        }
    });
}
function ajaxRequest(url,data,type) {
    if(type === "POST"){
        $.ajaxSetup({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        });
    }
    let response;
    $.ajax({
        url: url,
        type: type,
        data: data,
        dataType: 'json',
        async:false
    })
        .done(function(e){
            response = e;
        })
        .fail(function(e){
            response = e;
        });
    return response;
}
function swalFire(params) {
    Swal.fire({
        title:params.title,
        icon:params.type,
        confirmButtonText: 'Tamam',
    }).then((result)=>{
        if(result.isConfirmed){
            if(params.reload){
                location.reload()
            }
        }
    });
}
function updateProduct(params) {
    $(params.selector).on('click',function () {
        let form = $(params.form).serialize();
        form = form + "&product_id=" + params.product_id;
        let response = ajaxRequest(params.url,form,'POST');
        swalFire(response);
    })
}
