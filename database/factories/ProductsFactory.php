<?php

namespace Database\Factories;

use App\Models\Products;
use Exception;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class ProductsFactory extends Factory
{
    protected $model = Products::class;

    /**
     * Define the model's default state.
     *
     * @return array
     * @throws Exception
     */
    public function definition(): array
    {
        $name = $this->faker->name;
        return [
            'name' => $name,
            'name_slug' => Str::slug($name),
            'barcode' => random_int(100000000000000001,999999999999999869)
        ];
    }
}
