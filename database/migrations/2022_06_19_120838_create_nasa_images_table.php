<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNasaImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nasa_images', function (Blueprint $table) {
            $table->increments('nasa_image_id');
            $table->string('copyright');
            $table->timestamp('date');
            $table->text('explanation');
            $table->string('hd_url');
            $table->string('media_type');
            $table->string('service_version');
            $table->string('title');
            $table->string('url');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nasa_images');
    }
}
