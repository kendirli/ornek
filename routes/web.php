<?php

use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\Auth\LoginController;
use \App\Http\Controllers\HomeController;
use \App\Http\Controllers\ProductController;
use \App\Http\Controllers\NasaImagesController;

Route::get('/', function () {
    return view('welcome');
});

Route::post('/login',[LoginController::class,'login'])->name('login');
Route::group(['middleware' => ['useraccesscontrol']],function (){

    Route::get('/home', [HomeController::class, 'index'])->name('home');
    Route::get('/optimize', [HomeController::class, 'optimizeProject'])->name('optimize');

    Route::group(['prefix' => 'products'],function (){
        Route::get('/', [ProductController::class, 'index'])->name('product.list');
        Route::get('/edit/{product_id?}', [ProductController::class, 'edit'])->name('product.edit');
        Route::post('/update', [ProductController::class, 'update'])->name('product.update');
        Route::post('/delete', [ProductController::class, 'delete'])->name('product.delete');

    });
    Route::get('/logout', [LoginController::class, 'logout'])->name('logout.get');
    Route::group(['prefix' => 'nasa-images'],function (){
        Route::get('/', [NasaImagesController::class, 'index'])->name('nasa-images.index');
        Route::get('/download', [NasaImagesController::class, 'download'])->name('nasa-images.download');
    });
});
\Auth::routes();
