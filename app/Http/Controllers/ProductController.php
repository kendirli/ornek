<?php

namespace App\Http\Controllers;

use App\Models\Products;
use Illuminate\Http\Request;
use \Illuminate\Http\JsonResponse;
use Illuminate\Support\Str;

class ProductController extends Controller
{
    public function index()
    {
        $products = Products::query()
                            ->where('status',1)
                            ->get();
        return view('products.list',compact('products'));
    }

    public function edit(Request $request,$product_id)
    {
        $product = Products::findOrFail($product_id);
        return view('products.edit',compact('product'));
    }

    public function delete(Request $request): JsonResponse
    {
        $product_id = $request->input('product_id');

        $result = [
            'type' => 'error',
            'title' => 'Silme İşlemi Yapılamıyor'
        ];

        if(Products::query()->findOrFail($product_id)->delete()){
            $result = [
                'type' => 'success',
                'title' => 'Silme İşlemi Başarılı',
                'reload' => true
            ];
        }
        return response()->json($result);
    }

    public function update(Request $request): JsonResponse
    {
        $product_id = $request->input('product_id');
        $name = $request->input('name');
        $status = $request->input('status');
        if($status){
            $status = 1;
        }else{
            $status = 0;
        }
        $result = [
            'type' => 'error',
            'title' => 'Güncelleme İşlemi Yapılamıyor'
        ];
        $product = Products::where('product_id',$product_id)->update([
            'name' => $name,
            'name_slug' => Str::slug($name),
            'barcode' => $request->input('barcode'),
            'status' => $status
        ]);

        if($product){
            $result = [
                'type' => 'success',
                'title' => 'Güncelleme İşlemi Başarılı',
                'forward' => true
            ];
        }
        return response()->json($result);
    }
}
