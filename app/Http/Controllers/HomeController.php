<?php

namespace App\Http\Controllers;

use App\Models\Products;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;

class HomeController extends Controller
{
    public function index()
    {
        $productCount = Products::query()->where('status',1)->count();
        return view('home',compact('productCount'));
    }

    public function optimizeProject()
    {
        Artisan::call('optimize:clear');
        return redirect()->route('home');
    }
}
