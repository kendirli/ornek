<?php

namespace App\Http\Controllers;

use App\Models\NasaImages;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class NasaImagesController extends Controller
{

    public function index()
    {
        $nasaImages = NasaImages::query()
                                ->where('media_type','image')
                                ->get();
        return view('nasa-images.list',compact('nasaImages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\NasaImages  $nasaImages
     * @return \Illuminate\Http\Response
     */
    public function show(NasaImages $nasaImages)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\NasaImages  $nasaImages
     * @return \Illuminate\Http\Response
     */
    public function edit(NasaImages $nasaImages)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\NasaImages  $nasaImages
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, NasaImages $nasaImages)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\NasaImages  $nasaImages
     * @return \Illuminate\Http\Response
     */
    public function destroy(NasaImages $nasaImages)
    {
        //
    }

    public function download()
    {

    }
}
