<?php

namespace App\Console\Commands;

use App\Models\NasaImages;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class NasaImageApi extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get:NasaImageApi {download?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Nasa dan Resimleri alıp database ve locale kaydetme';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        #TODO Sonradan düzenleme yapılacak...
        ini_set('memory_limit', -1);
        set_time_limit(0);

        $download = false;
        if($this->argument('download')){
            $download = true;
        }
        try {
            $client = new Client();
            $response = $client->get(config('project.NASA_APOD'));
            if($response->getStatusCode() == '200' &&  ($response->getBody())){
                $bodyData = json_decode($response->getBody());
                $newData = [];
                foreach ($bodyData as $data){
                    $newData[]=[
                        'copyright' => $data->copyright ?? null,
                        'date' => $data->date,
                        'explanation' => $data->explanation,
                        'hd_url' => $data->hdurl ?? null,
                        'media_type' => $data->media_type,
                        'service_version' => $data->service_version,
                        'title' => $data->title,
                        'url' => $data->url ?? null
                    ];
                }
                $result = NasaImages::query()->insert($newData);
                if($result){
                    $this->line('Kayıt Başarılı:');
                    if($download){
                        $this->line('Resimler indiriliyor');
                        $nasaImages = NasaImages::query()->where('media_type','image')->whereNotNull('url')->pluck('url');
                        $nasaImages->each(function ($item){
                            $urlExplode = explode('/',$item);
                            $fileName = $urlExplode[array_key_last($urlExplode)];
                            Storage::disk('nasa-image')->put($fileName,file_get_contents($item));
                        });
                        $this->line('Indirme Islemi Tamam');
                    }
                }else{
                    $this->line('Kayıt Hatası:');
                }
            }else{
                $this->line('Servis Hatası:'.$response->getStatusCode());
            }
        }catch (\Exception $e){
            Log::critical('Nasa Image Job Hatası:'.$e->getMessage());
            $this->line('Hata'.$e->getMessage());
        } catch (GuzzleException $e) {
            Log::critical('GuzzleException:'.$e->getMessage());
        }
    }
}
