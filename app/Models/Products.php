<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Products extends Model
{
    use HasFactory,SoftDeletes;
    protected $primaryKey = 'product_id';
    protected $fillable = [
        'name','name_slug','barcode','status',
    ];
    protected $dates = [
        'deleted_at','created_at','updated_at'
    ];
}
