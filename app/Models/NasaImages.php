<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NasaImages extends Model
{
    use HasFactory,SoftDeletes;
    protected $table = 'nasa_images';
    protected $primaryKey = 'nasa_image_id';
    protected $dates = [
      'date','deleted_at','created_at','updated_at'
    ];
    protected $guarded = [
      'nasa_image_id'
    ];
}
