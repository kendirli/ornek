<?php

use \Illuminate\Support\Facades\Request;

function getUrlActiveControl($route): string
{
    $result = "";
    if($route == Request::route()->getName()){
        $result = "active";
    }
    return $result;
}

function removeExtension($fileName): string
{
    return pathinfo($fileName, PATHINFO_FILENAME);
}
