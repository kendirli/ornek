<!-- Main Footer -->
<footer class="main-footer">
    <strong>Copyright &copy; {{\Carbon\Carbon::now()->format('Y')}} <a href="#">SeFu</a>.</strong>
    Tüm Hakları saklıdır
    <div class="float-right d-none d-sm-inline-block">
        <b>Version</b> 0.01
    </div>
</footer>
