<!DOCTYPE html>
<html lang="tr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <title>{{ $title ?? config('config.APP_NAME')}}</title>
    @include('layouts.styles')
    @stack('styles')
</head>
<body class="hold-transition dark-mode sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
<div class="wrapper">
    <!-- Preloader -->
    <div class="preloader flex-column justify-content-center align-items-center">
        <img class="animation__wobble" src="{{asset('dist/img/AdminLTELogo.png')}}" alt="AdminLTELogo" height="60" width="60">
    </div>
    @include('layouts.header')
    @include('layouts.menu')
    @yield('content')
    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
    @include('layouts.footer')
</div>
<!-- ./wrapper -->
@include('layouts.scripts')
@stack('plugins')
@stack('js')
<script>
    console.log("{{\Illuminate\Support\Facades\Request::route()->getName()}}");
</script>
</body>
</html>
