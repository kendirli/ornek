@extends('layouts.master')
@push('styles')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/datatables-buttons/css/buttons.bootstrap4.min.css')}}">
    <!-- Ekko Lightbox -->
    <link rel="stylesheet" href="{{asset('/plugins/ekko-lightbox/ekko-lightbox.css')}}">
@endpush
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Nasa Images</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{route('home')}}">Anasayfa</a></li>
                            <li class="breadcrumb-item active">Nasa Images</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Nasa Resim Listeleme Sayfası</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <table id="nasaImages" class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th style="width: 5%;">#</th>
                                        <th style="width: 10%;">Copyright</th>
                                        <th>Explanation</th>
                                        <th>Image</th>
                                        <th>Oluşturma Tarihi</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($nasaImages as $image)
                                        @php
                                            $urlExplode = explode('/',$image->url);
                                            $fileName = $urlExplode[array_key_last($urlExplode)];
                                            $explanation = \Illuminate\Support\Str::limit($image->explanation,500);
                                        @endphp
                                        <tr>
                                            <td>{{($loop->index+1)}}</td>
                                            <td>{{$image->copyright}}</td>
                                            <td>
                                                {{$explanation}}
                                            </td>
                                            <td>
                                                <a href="{{ asset('storage/nasa_image/'.$fileName) }}" data-toggle="lightbox" data-title="{{removeExtension($fileName)}}">
                                                    <img src="{{ asset('storage/nasa_image/'.$fileName) }}" class="img-fluid mb-2" alt="black sample"/>
                                                </a>
                                            </td>
                                            <td>{{$image->date->format('d-m-Y')}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection
@push('plugins')
    <!-- Ekko Lightbox -->
    <script src="{{asset('/plugins/ekko-lightbox/ekko-lightbox.min.js')}}"></script>
@endpush
@push('js')
    <script type="text/javascript">
        $(document).on('click', '[data-toggle="lightbox"]', function(event) {
            event.preventDefault();
            $(this).ekkoLightbox({
                alwaysShowClose: true
            });
        });
    </script>
@endpush
