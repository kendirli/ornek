@extends('layouts.master')
@section('content')
<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1> </h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item">
                            <a href="{{route('home')}}">Anasayfa</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="{{route('product.list')}}">Ürünler</a>
                        </li>
                        <li class="breadcrumb-item active">{{$product->name}}</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="card card-danger">
                        <div class="card-header">
                            <h3 class="card-title">Ürün "{{$product->name}}" Düzenleme</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form id="product_form">
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="name">Ürün Adı</label>
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Ürün Adı" value="{{$product->name}}">
                                </div>
                                <div class="form-group">
                                    <label for="barcode">Barkod</label>
                                    <input type="number" class="form-control" id="barcode" name="barcode" placeholder="Barkod" value="{{$product->barcode}}">
                                </div>
                                <div class="form-group">
                                    <label>
                                        <input id="status" type="checkbox" name="status" @if($product->status === 1) checked @endif
                                        data-bootstrap-switch data-off-color="danger" data-on-color="success" data-on-text="Aktif" data-off-text="Pasif">
                                    </label>
                                </div>
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="button" class="btn btn-success" id="updateProductButton">Güncelle</button>
                                <a href="{{route('product.list')}}" class="btn btn-danger">İptal</a>
                            </div>
                        </form>
                    </div>
                    <!-- /.card -->
                </div>
                <!--/.col (left) -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
@endsection
@push('js')
    <script>
        updateProduct({
            selector:'#updateProductButton',
            form:'#product_form',
            url:'{{route('product.update')}}',
            product_id:'{{$product->product_id}}'
        });
    </script>
@endpush
