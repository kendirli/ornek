@extends('layouts.master')
@push('styles')
<!-- DataTables -->
    <link rel="stylesheet" href="{{asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/datatables-buttons/css/buttons.bootstrap4.min.css')}}">
@endpush
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Ürünler</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{route('home')}}">Anasayfa</a></li>
                            <li class="breadcrumb-item active">Ürünler</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Ürün Listeleme Sayfası</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <table id="products" class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th style="width: 5%;">#</th>
                                            <th>Ürün Adı</th>
                                            <th><i class="fa fa-barcode"></i> Barkod</th>
                                            <th>Oluşturma Tarihi</th>
                                            <th style="width: 8%;">İşlem</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($products as $product)
                                            <tr>
                                                <td>{{($loop->index+1)}}</td>
                                                <td>{{$product->name}}</td>
                                                <td>{{$product->barcode}}</td>
                                                <td>{{$product->created_at->format('d-m-Y H:i:s')}}</td>
                                                <td>
                                                    <a href="{{route('product.edit',['product_id' => $product->product_id])}}" class="btn btn-warning">
                                                      <i class="fa fa-edit"></i>
                                                    </a>
                                                    <a class="btn btn-danger" onclick="return swalSweetAlert({
                                                        forwardUrl:'{{route('product.delete')}}',
                                                        data:{
                                                            product_id:'{{$product->product_id}}'
                                                        }
                                                    });">
                                                      <i class="fa fa-trash"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection
@push('plugins')
@endpush
@push('js')
<script type="text/javascript">
</script>
@endpush
